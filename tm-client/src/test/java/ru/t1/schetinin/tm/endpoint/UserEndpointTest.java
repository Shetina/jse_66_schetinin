package ru.t1.schetinin.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.schetinin.tm.api.endpoint.IUserEndpoint;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.dto.request.*;
import ru.t1.schetinin.tm.marker.SoapCategory;
import ru.t1.schetinin.tm.dto.model.UserDTO;
import ru.t1.schetinin.tm.service.PropertyService;

import java.util.Locale;

@Category(SoapCategory.class)
public class UserEndpointTest {
    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin("admin");
        loginRequest.setPassword("admin");
        adminToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin("userTestLogin");
        request.setPassword("userTestPassword");
        request.setEmail("testEmail");
        USER_ENDPOINT.registryUser(request);
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin("userTestLogin");
        USER_ENDPOINT.removeUser(request);
    }

    @Nullable
    private String getUserToken(final String login, final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return AUTH_ENDPOINT.login(request).getToken();
    }

    @Test
    public void changePasswordUser() {
        @Nullable String token = getUserToken("userTestLogin", "userTestPassword");
        Assert.assertNotNull(token);
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(token);
        request.setPassword("userTestPassword".toLowerCase(Locale.ROOT));
        Assert.assertNotNull(USER_ENDPOINT.changeUserPassword(request));
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(token);
        AUTH_ENDPOINT.logout(logoutRequest);
        token = getUserToken("userTestLogin", "userTestPassword".toLowerCase(Locale.ROOT));
        Assert.assertNotNull(token);
        @NotNull final UserChangePasswordRequest newPasswordRequest = new UserChangePasswordRequest(token);
        newPasswordRequest.setPassword("userTestPassword");
        Assert.assertNotNull(USER_ENDPOINT.changeUserPassword(newPasswordRequest));
    }

    @Test
    public void lockUser() {
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken);
        request.setLogin("userTestLogin");
        Assert.assertNotNull(USER_ENDPOINT.lockUser(request));
        Assert.assertThrows(Exception.class, () -> getUserToken("userTestLogin", "userTestPassword"));
        unlockUser();
    }

    @Test
    public void registryUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin("userTestLogin");
        USER_ENDPOINT.removeUser(removeRequest);
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin("userTestLogin");
        request.setPassword("userTestPassword");
        request.setEmail("testEmail");
        USER_ENDPOINT.registryUser(request);
        Assert.assertNotNull(getUserToken("userTestLogin", "userTestPassword"));
    }

    @Test
    public void removeUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken);
        removeRequest.setLogin("userTestLogin");
        Assert.assertNotNull(USER_ENDPOINT.removeUser(removeRequest));
        Assert.assertThrows(Exception.class, () -> getUserToken("userTestLogin", "userTestPassword"));
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin("userTestLogin");
        request.setPassword("userTestPassword");
        request.setEmail("testEmail");
        USER_ENDPOINT.registryUser(request);
    }

    @Test
    public void unlockUser() {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(adminToken);
        request.setLogin("userTestLogin");
        Assert.assertNotNull(USER_ENDPOINT.unlockUser(request));
        Assert.assertNotNull(getUserToken("userTestLogin", "userTestPassword"));
    }

    @Test
    public void updateProfileUser() {
        @Nullable final String token = getUserToken("userTestLogin", "userTestPassword");
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(token);
        request.setFirstName("userTestFirstName");
        request.setLastName("userTestLastName");
        request.setMiddleName("userTestMiddleName");
        Assert.assertNotNull(USER_ENDPOINT.updateUserProfile(request));
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(token);
        @Nullable final UserDTO user = AUTH_ENDPOINT.profile(viewRequest).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("userTestFirstName", user.getFirstName());
        Assert.assertEquals("userTestLastName", user.getLastName());
        Assert.assertEquals("userTestMiddleName", user.getMiddleName());
    }

}
