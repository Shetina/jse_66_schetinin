package ru.t1.schetinin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable final String login, @Nullable final String password) throws Exception;

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws Exception;

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws Exception;

    @Nullable
    User findByLogin(@Nullable final String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable final String email) throws Exception;

    Boolean isLoginExists(@Nullable final String login) throws Exception;

    Boolean isEmailExists(@Nullable final String email) throws Exception;

    void lockUserByLogin(@Nullable final String login) throws Exception;

    void removeByLogin(@Nullable final String login) throws Exception;

    void setPassword(@Nullable final String id, @Nullable final String password) throws Exception;

    void unlockUserByLogin(@Nullable final String login) throws Exception;

    void updateUser(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) throws Exception;

}