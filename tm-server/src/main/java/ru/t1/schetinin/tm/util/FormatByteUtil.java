package ru.t1.schetinin.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;

@NoArgsConstructor
public final class FormatByteUtil {

    private static final long KILOBYTE = 1024;

    private static final long MEGABYTE = KILOBYTE * KILOBYTE;

    private static final long GIGABYTE = MEGABYTE * KILOBYTE;

    private static final long TERABYTE = GIGABYTE * KILOBYTE;

    private static final long[] DIVIDERS = new long[]{TERABYTE, GIGABYTE, MEGABYTE, KILOBYTE, 1};

    @NotNull
    private static final String[] UNITS = new String[]{"TB", "GB", "MB", "KB", "B"};

    @NotNull
    private static String format(final long value, final long divider, @NotNull final String unit) {
        final double result =
                divider > 1 ? (double) value / (double) divider : (double) value;
        return new DecimalFormat("#,##0.#").format(result) + " " + unit;
    }

    @Nullable
    public static String formatByte(final long value) {
        if (value < 1)
            throw new IllegalArgumentException("Invalid file size: " + value);
        @Nullable String result = null;
        for (int i = 0; i < DIVIDERS.length; i++) {
            final long divider = DIVIDERS[i];
            if (value >= divider) {
                result = format(value, divider, UNITS[i]);
                break;
            }
        }
        return result;
    }

}